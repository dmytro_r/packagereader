export default function PackageResolver({data}) {
    const flatten = (inputData, flatList = [], depth = 1) => {

        let row = {name: inputData.name, ver: inputData.version, depth: depth}

        flatList.push(row)
        if(inputData.dependencies) {
            let packages = Object.keys(inputData.dependencies)
            packages.map(
                packageName => {

                    if(typeof inputData.dependencies[packageName] === 'string') {
                        flatList.push({name: packageName, ver: inputData.dependencies[packageName], depth: depth + 1})
                    } else {
                        flatten(inputData.dependencies[packageName], flatList, depth + 1 )
                    }

                }
            )
        }
        return flatList
    }

    const resolvePackageVersion = (packageName, flatPackageList) => {

        let version = 0
        let versionDepth = +Infinity

        flatPackageList.forEach( pack => {
            if(pack.name === packageName) {
                if(pack.depth < versionDepth ) {
                    versionDepth = pack.depth
                    version = pack.ver
                }
            }
        })

        return {name: packageName, ver: version}
    }

    let flattenPackageList = flatten(data)

    let packageNames = new Set()
    flattenPackageList.map( pack => packageNames.add(pack.name) )

    let resolvedPackages = Array.from(packageNames).map( pack => resolvePackageVersion(pack, flattenPackageList) )
    return (
        <ul>
            {resolvedPackages.map( p => <li>{p.name} - {p.ver}</li>)}
        </ul>
    )
}