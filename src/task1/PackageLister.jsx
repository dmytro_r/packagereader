import {useEffect, useState} from 'react'

import './PackageLister.css'

export default function PackageLister({data}) {
    let [open, setOpen] = useState(true)

    const toggleOpen = () => {
        setOpen(!open)
    }
    const className = data.dependencies ? (open ? 'open': 'closed') : ''
    return (
        <ul className={className}>
            <li onClick={toggleOpen}>{data.name} - {data.version} {data.dependencies ? (open ? <>&darr;</> : <>&uarr;</>) : null}</li>
            {data.dependencies && Object.keys(data.dependencies).map(key => {
                let record = data.dependencies[key]
                if(typeof record === 'string') {
                    record = {name: key, version: record}
                }
                    return <PackageLister data={record}/>})}
        </ul>
    )
}