import PackageLister from './task1/PackageLister'
import PackageResolver from './task2/PackageResolver'
import {useEffect, useState} from 'react'
import './App.css';
import data from './task1/data.json'

function App() {


    return (
        <div className="App">
            <PackageLister data={data}/>
            <PackageResolver data={data}/>
        </div>
    );
}

export default App;
